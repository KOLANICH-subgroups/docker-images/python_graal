#!/usr/bin/env python3
import typing
import sys
import tempfile
from io import StringIO, BytesIO
from pathlib import Path, PurePath
import _io
import enum
from urllib.parse import urlparse
import os

import apt
import gpg
import requests
from plumbum import cli
import lsb_release


class SecurityIssues(enum.IntFlag):
	expired = 1
	disabled = 2
	revoked = 4
	invalid = 8
	brokenCrypto = 16
	keyLengthIsTooShort = 32


class SignAlgo(enum.IntEnum):
	RSA_encrypt_sign = 1
	RSA_sign = 3
	ElGamal = 16
	DSA = 17
	ECDSA = 19
	EdDSA = 22
	AEDSA = 24


class HashAlgo(enum.IntEnum):
	sha256 = 8
	sha384 = 9
	sha512 = 10


def isConsideredInsecure(k):
	res = k.invalid * SecurityIssues.invalid | k.disabled * SecurityIssues.disabled | k.expired * SecurityIssues.expired | k.revoked * SecurityIssues.revoked
	for sk in k.subkeys:
		res |= isSubkeyConsideredInsecure(sk)
	return res


def isSubkeyConsideredInsecure(k):
	res = k.invalid * SecurityIssues.invalid | k.disabled * SecurityIssues.disabled | k.expired * SecurityIssues.expired | k.revoked * SecurityIssues.revoked
	try:
		algo = SignAlgo(k.pubkey_algo)
	except ValueError:
		res |= SecurityIssues.brokenCrypto
		return res

	minimumLegths = {
		SignAlgo.RSA_encrypt_sign: 2048,
		SignAlgo.RSA_sign: 2048,
		SignAlgo.ElGamal: 2048,
		SignAlgo.DSA: 2048,
		SignAlgo.ECDSA: 2048
	}

	if algo in minimumLegths and k.length < minimumLegths[algo]:
		res |= SecurityIssues.keyLengthIsTooShort
	return res

def getTrustedFilePath():
	res = apt.apt_pkg.config.find_file("Apt::GPGV::TrustedKeyring")
	if not res:
		res = apt.apt_pkg.config.find_file("Dir::Etc::Trusted")
		if not res:
			res = "/etc/apt/trusted.gpg"
	return Path(res)


trustedFilePath = getTrustedFilePath()

tempCtx = gpg.Context(armor=True, offline=True)


def getTrustedParts():
	return Path(apt.apt_pkg.config.find_dir("Dir::Etc::TrustedParts"))


def getSourceParts():
	return Path(apt.apt_pkg.config.find_dir("Dir::Etc::sourceparts"))


trustedPartsDir = getTrustedParts()
sourcesListsDir = getSourceParts()


def appendKey(trustedFilePath, key2add: bytes):
	with tempfile.TemporaryDirectory(dir="./") as d:
		dp = Path(d)
		ctx = gpg.Context(offline=True, home_dir=d)
		imps = ctx.key_import(trustedFilePath.read_bytes())
		if not imps:
			raise Exception("Main keys are not imported")
		imps = ctx.key_import(key2add)
		if not imps:
			raise Exception("Additional key is not imported")
		trustedFilePath.write_bytes(ctx.key_export())


def addRepo(repoHumanName, repoURI, repoPublicKeyURI, repoPublicKeyFingerprint, arch="amd64", release=None, channel="contrib"):
	if release is None:
		release = lsb_release.get_distro_information()["CODENAME"]
	keyBytes = requests.get(repoPublicKeyURI).content
	if not checkKeyFingerprint(keyBytes, repoPublicKeyFingerprint):
		raise Exception("No keys were imported")
	#appendKey(trustedFilePath, keyBytes)
	keyPath = trustedPartsDir / (repoHumanName + ".gpg")
	keyPath.write_bytes(keyBytes)
	listFileName = sourcesListsDir / (repoHumanName + ".list")
	strToWrite = "deb [" + ("arch=" + arch + "," if arch else "") + "signed-by=" + str(keyPath) + "] " + repoURI + " " + release + " " + channel
	listFileName.write_text(strToWrite)


def checkKeyFingerprint(keyBytes, fp):
	fp = fp.upper()
	imps = tempCtx.key_import(keyBytes)
	print("imps", imps)
	j = 0
	for ik in imps.imports:
		k = tempCtx.get_key(ik.fpr)
		insecurity = isConsideredInsecure(k)
		if insecurity:
			raise Exception("Key " + k.fpr + " ( " + generateHumanName(k) + " ) from " + str(kf) + " is considered insecure (" + str(insecurity) + ")!")
		if ik.fpr != fp:
			raise Exception("The key has fingerprint " + ik.fpr + " but the requested fingerprint was " + fp)
		j += 1
	return j


def addOurRepo(repoHumanName, ourRepoURI, repoPublicKeyFingerprint, arch="amd64", release=None, channel="contrib"):
	addRepo(repoHumanName, ourRepoURI + "/repo", ourRepoURI + "/public.gpg", repoPublicKeyFingerprint, arch=arch, release=release, channel=channel)


class AdderCLI(cli.Application):
	arch = cli.SwitchAttr("--arch", default="amd64")
	release = cli.SwitchAttr("--release", default=None)
	channel = cli.SwitchAttr("--channel", default="contrib")

	def main(self, repoHumanName, ourRepoURI, repoPublicKeyFingerprint):
		addOurRepo(repoHumanName, ourRepoURI, repoPublicKeyFingerprint, arch=self.arch, release=self.release, channel=self.channel)


if __name__ == "__main__":
	AdderCLI.run()  # addOurRepo("vanilla_CMake_KOLANICH", "https://kolanich-subgroups.gitlab.io/packages/CMake_deb", "0e94c991ee6dfe96affa14a8a059ada6ca7b4a3f")
