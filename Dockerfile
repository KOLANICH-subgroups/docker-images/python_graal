# syntax=docker/dockerfile:experimental
FROM registry.gitlab.com/kolanich-subgroups/docker-images/fixed_python:latest
LABEL maintainer="KOLANICH"
WORKDIR /tmp
ADD ./*.sh ./
ADD ./*.py ./
ENV JAVA_HOME /usr/lib/jvm/graalvm-ce-amd64
#TODO pip3 install --upgrade --pre git+https://github.com/jpype-project/JPype && \
RUN  \
  set -ex;\
  ls -lh;\
  apt-get update;\
  apt-get install -y --no-install-recommends python3-gpg python3-apt;\
  python3 fix_python_modules_paths.py;\
  python3 ./add_repo.py "graal_KOLANICH" "https://kolanich-subgroups.gitlab.io/packages/GraalVM_deb" "898bad1e937da3e70035b48a27805fb291a720d1";\
  apt-get update;\
  apt-get install --upgrade -y graalvm graalvm-gu graalvm-python graalvm-polyglot;\
  export JAVA_HOME=/usr/lib/jvm/graalvm-ce-amd64;\
  export PATH=$PATH:$JAVA_HOME/bin;\
  java -version;\
  gu install --no-progress -o python;\
  pip install --upgrade --pre JPype1;\
  apt-get autoremove --purge -y;\
  apt-get clean;\
  rm -rf /var/lib/apt/lists/*;\
  rm -r ~/.cache/pip ;\
  rm -r /tmp/*
ENV PATH $PATH:$JAVA_HOME/bin
